<?php

namespace App\Http\Controllers;

use App\Mail\MalasngodingEmail;
use Illuminate\Support\Facades\Mail;

class MalasngodingController extends Controller
{
    public function index()
    {

        Mail::to("testing@konohagakure.co.jp")->send(new MalasngodingEmail());

        return "Email telah dikirim";

    }

}
