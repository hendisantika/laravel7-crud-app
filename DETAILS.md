# Laravel CRUD APP

### Things to do list:
1. Clone this repository: `https://gitlab.com/hendisantika/laravel7-crud-app.git`
2. Go inside the folder: `cd laravel7-crud-app`
3. Run `cp .env.example .env` & set your desired db name
4. Run `composer install` 
5. Run `php artisan passport:keys` 
6. Run `php artisan key:generate` 
7. Run `php artisan migrate`
9. Run `php artisan db:seed`
9. Run `composer require laravel/ui`
10. Run `php artisan ui bootstrap`
11. Run `npm install && npm run dev`
12. Run `php artisan serve`

Open your favorite browser: http://localhost:8000/contacts

### Screen shot

Add New Contact

![Add New Contact](img/add.png "Add New Contact")

Update Contact

![Update Contact](img/update.png "Update Contact")

List All Contacts

![List All Contacts](img/list.png "List All Contacts")

Login Page

![Login Page](img/login.png "Login Page")

Register Page

![Register Page](img/register.png "Register Page")

Forgot Email

![Forgot Email](img/reset.png "Forgot Email")

Reset Password

![Reset Password](img/reset2.png "Reset Password")


Confirm Reset Password

![Confirm Reset Password](img/reset3.png "Confirm Reset Password")

Confirm Reset Password Success

![Confirm Reset Password Success](img/reset4.png "Confirm Reset Password Success")

Send Email

![Send Email](img/email1.png "Send Email")

Send Email

![Send Email](img/email2.png "Send Email2")

### Heroku

This repository is deployed to heroku with this link: http://laravel7-crud-app.herokuapp.com/contacts
